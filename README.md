# Skeleton
#### 项目介绍
- 项目名称：Skeleton
- 所属系列：OpenHarmony的第三方组件适配移植
- 功能：列表展示，Loadding圈，加载列表时骨架屏效果，网格列表展示
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：master分支

#### 效果演示

![img](gif/skeleton.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```gradle
allprojects {
	repositories {
		maven {
			url 'https://s01.oss.sonatype.org/content/repositories/releases/'
		}
	}
}
```
2.在entry模块的build.gradle文件中，
```gradle
dependencies {
	implementation('com.gitee.chinasoft_ohos:Skeleton:1.0.0')
	......  
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1. 对于ListContainer:
  ```java
  skeletonScreen = Skeleton.bind(listContainer)
                                .adapter(adapter)
                                .load(ResourceTable.Layout_item_skeleton_news)
                                .show();
  ```
2. 对于Component:
  ```java
  skeletonScreen = Skeleton.bind(mScrollView)
                                .load(ResourceTable.Layout_dialog)
                                .show();
  ```

3. 更多的配置:
  ```java
  .shimmer(true)      // 是否显示闪烁动画                      默认为：true
  .count(10)          // ListContainer展示项目个数。           默认为：10
  .color(color)       // 闪烁动画颜色                          默认为：#FFDAD3D3
  .angle(20)          // 闪烁动画倾斜角度                      默认为：20
  .duration(1000)     // 闪烁动画时长                    	 默认为：1000
  .frozen(false)      // 在骨架显示期间是否冻结ListContainer    默认为： true
  ```

4. 当数据返回时，您可以调用该方法来隐藏骨架加载视图
  ```java
  skeletonScreen.hide()
  ```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

1.0.0
