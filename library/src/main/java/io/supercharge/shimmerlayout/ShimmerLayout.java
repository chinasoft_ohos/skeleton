package io.supercharge.shimmerlayout;

import com.ethanhua.skeleton.ResourceTable;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.Image;
import ohos.agp.components.StackLayout;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.GroupShader;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.PixelMapShader;
import ohos.agp.render.Shader;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * ShimmerLayout
 *
 * @since 2021-04-16
 */
public class ShimmerLayout extends StackLayout implements Component.DrawTask {
    private static final int DEFAULT_ANIMATION_DURATION = 1500;

    private static final byte DEFAULT_ANGLE = 20;

    private static final byte MIN_ANGLE_VALUE = -45;
    private static final byte MAX_ANGLE_VALUE = 45;
    private static final byte MIN_MASK_WIDTH_VALUE = 0;
    private static final byte MAX_MASK_WIDTH_VALUE = 1;

    private static final byte MIN_GRADIENT_CENTER_COLOR_WIDTH_VALUE = 0;
    private static final byte MAX_GRADIENT_CENTER_COLOR_WIDTH_VALUE = 1;
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ShimmerLayout");
    private static final float MASK_WIDTH_DEF_VALUE_FLOAT = 0.5f;
    private static final float SHIMMER_GRADIENT_CENTER_COLOR_WIDTH = 0.1f;
    private static final int POINT = 2;
    private static final int DISTRIBUTION4 = 4;
    private static final int DISTRIBUTION3 = 3;
    private static final int DISTRIBUTION2 = 2;
    private static final float DISTRIBUTION_FLOAT1 = 0.5f;
    private static final float DISTRIBUTION_FLOAT2 = 2f;

    private int maskOffsetX;
    private Rect maskRect;
    private Paint gradientTexturePaint;
    private AnimatorValue maskAnimator;

    private PixelMap localMaskBitmap;
    private PixelMap maskBitmap;

    private boolean isAnimationReversed;
    private boolean isAnimationStarted;
    private boolean isAutoStart;
    private int shimmerAnimationDuration;
    private int shimmerColor;
    private int shimmerAngle;
    private float maskWidth;
    private float gradientCenterColorWidth;
    private int childBackgroundColor;

    private ComponentTreeObserver.WindowBoundListener startAnimationPreDrawListener;

    private int layerId;

    private boolean isAllImage = true;
    private ArrayList<Image> images = new ArrayList<>();

    private ComponentContainer mComponentContainer;
    private Paint mPaint;

    /**
     * ShimmerLayout
     *
     * @param context
     * @param attrSet
     */
    public ShimmerLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * ShimmerLayout
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public ShimmerLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        ohos.global.resource.ResourceManager resManager = context.getResourceManager();
        shimmerAngle = TypedAttrUtils.getInteger(attrSet, "shimmer_angle", DEFAULT_ANGLE);

        shimmerAnimationDuration = TypedAttrUtils.getInteger(attrSet, "shimmer_animation_duration",
                DEFAULT_ANIMATION_DURATION);
        try {
            shimmerColor = TypedAttrUtils.getIntColor(attrSet, "shimmer_color",
                    resManager.getElement(ResourceTable.Color_shimmer_color).getColor());

            childBackgroundColor = TypedAttrUtils.getIntColor(attrSet, "child_background_color",
                    resManager.getElement(ResourceTable.Color_background_color).getColor());
        } catch (IOException | WrongTypeException | NotExistException e) {
            e.getMessage();
        }
        isAutoStart = TypedAttrUtils.getBoolean(attrSet, "shimmer_auto_start", false);
        maskWidth = TypedAttrUtils.getFloat(attrSet, "shimmer_mask_width", MASK_WIDTH_DEF_VALUE_FLOAT);
        gradientCenterColorWidth = TypedAttrUtils.getFloat(attrSet,
                "shimmer_gradient_center_color_width", SHIMMER_GRADIENT_CENTER_COLOR_WIDTH);
        isAnimationReversed = TypedAttrUtils.getBoolean(attrSet, "shimmer_reverse_animation", false);

        HiLog.error(LABEL, "childBackgroundColor = " + childBackgroundColor);

        mPaint = new Paint();
        setMaskWidth(maskWidth);
        setGradientCenterColorWidth(gradientCenterColorWidth);
        setShimmerAngle(shimmerAngle);
        if (isAutoStart && getVisibility() == VISIBLE) {
            startShimmerAnimation();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        dispatchDrawChildView(canvas, 0);

        RectFloat rectFloat = new RectFloat(0, 0, this.getWidth(), this.getHeight());
        layerId = canvas.saveLayer(rectFloat, mPaint);
        Color color = new Color(childBackgroundColor);
        mPaint.setColor(color);
        dispatchDrawChildView(canvas, 1);
        dispatchDrawShimmer(canvas);
        canvas.restoreToCount(layerId);
    }

    private void dispatchDrawChildView(Canvas canvas, int type) {
        for (int mInt = 0; mInt < mComponentContainer.getChildCount(); mInt++) {
            Component component = mComponentContainer.getComponentAt(mInt);
            if (component instanceof Image) {
                Image image = (Image) component;
                images.add(image);
                canvas.drawTexture(image.getMarginLeft(), image.getMarginTop(), new Texture(image.getPixelMap()));
            } else if (type == 1) {
                isAllImage = false;
                canvas.drawRect(component.getMarginLeft(), component.getMarginTop(),
                        component.getWidth() + component.getMarginLeft(),
                        component.getHeight() + component.getMarginTop(), mPaint);
            }
        }
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == VISIBLE) {
            if (isAutoStart) {
                startShimmerAnimation();
            }
        } else {
            stopShimmerAnimation();
        }
    }

    /**
     * 启动微光动画
     */
    public void startShimmerAnimation() {
        if (isAnimationStarted) {
            return;
        }

        if (getWidth() == 0) {
            startAnimationPreDrawListener = new ComponentTreeObserver.WindowBoundListener() {
                @Override
                public void onWindowBound() {
                    startShimmerAnimation();
                }

                @Override
                public void onWindowUnbound() {
                    resetShimmering();
                }
            };

            getComponentTreeObserver().addWindowBoundListener(startAnimationPreDrawListener);

            return;
        }

        Animator animator = getShimmerAnimation();
        animator.start();
        isAnimationStarted = true;
    }

    /**
     * 停止微光动画
     */
    public void stopShimmerAnimation() {
        if (startAnimationPreDrawListener != null) {
            getComponentTreeObserver().removeWindowBoundListener(startAnimationPreDrawListener);
        }

        resetShimmering();
    }

    /**
     * 设置闪烁颜色
     *
     * @param shimmerColor
     */
    public void setShimmerColor(int shimmerColor) {
        this.shimmerColor = shimmerColor;
        resetIfStarted();
    }

    /**
     * 设置微光动画持续时间
     *
     * @param durationMillis
     */
    public void setShimmerAnimationDuration(int durationMillis) {
        this.shimmerAnimationDuration = durationMillis;
        resetIfStarted();
    }

    /**
     * 设置动画反转
     *
     * @param isReversed
     */
    public void setAnimationReversed(boolean isReversed) {
        this.isAnimationReversed = isReversed;
        resetIfStarted();
    }

    /**
     * 设置背景颜色
     *
     * @param backgroundColor
     */
    public void setChildBackgroundColor(int backgroundColor) {
        this.childBackgroundColor = backgroundColor;
        resetIfStarted();
    }

    /**
     * Set the angle of the shimmer effect in clockwise direction in degrees.
     * The angle must be between {@value #MIN_ANGLE_VALUE} and {@value #MAX_ANGLE_VALUE}.
     *
     * @param angle The angle to be set
     * @throws IllegalArgumentException
     */
    public void setShimmerAngle(int angle) {
        if (angle < MIN_ANGLE_VALUE || angle > MAX_ANGLE_VALUE) {
            throw new IllegalArgumentException(String.format("shimmerAngle value must be between %d and %d",
                    MIN_ANGLE_VALUE,
                    MAX_ANGLE_VALUE));
        }
        this.shimmerAngle = angle;
        resetIfStarted();
    }

    /**
     * Sets the width of the shimmer line to aa value higher than 0 to less or equal to 1.
     * 1 means the width of the shimmer line is equal to half of the width of the ShimmerLayout.
     * The default value is 0.5.
     *
     * @param maskWidth The width of the shimmer line.
     * @throws IllegalArgumentException
     */
    public void setMaskWidth(float maskWidth) {
        if (maskWidth <= MIN_MASK_WIDTH_VALUE || maskWidth > MAX_MASK_WIDTH_VALUE) {
            throw new IllegalArgumentException(
                    String.format("maskWidth value must be higher than %d and less or equal to %d",
                            MIN_MASK_WIDTH_VALUE, MAX_MASK_WIDTH_VALUE));
        }

        this.maskWidth = maskWidth;
        resetIfStarted();
    }

    /**
     * Sets the width of the center gradient color to aa value higher than 0 to less than 1.
     * 0.99 means that the whole shimmer line will have this color with aa little transparent edges.
     * The default value is 0.1.
     *
     * @param gradientCenterColorWidth The width of the center gradient color.
     * @throws IllegalArgumentException
     */
    public void setGradientCenterColorWidth(float gradientCenterColorWidth) {
        if (gradientCenterColorWidth <= MIN_GRADIENT_CENTER_COLOR_WIDTH_VALUE
                || gradientCenterColorWidth >= MAX_GRADIENT_CENTER_COLOR_WIDTH_VALUE) {
            throw new IllegalArgumentException(
                    String.format("gradientCenterColorWidth value must be higher than %d and less than %d",
                            MIN_GRADIENT_CENTER_COLOR_WIDTH_VALUE, MAX_GRADIENT_CENTER_COLOR_WIDTH_VALUE));
        }

        this.gradientCenterColorWidth = gradientCenterColorWidth;
        resetIfStarted();
    }

    private void resetIfStarted() {
        if (isAnimationStarted) {
            resetShimmering();
            startShimmerAnimation();
        }
    }

    private void dispatchDrawShimmer(Canvas canvas) {
        localMaskBitmap = getMaskBitmap();
        if (localMaskBitmap == null) {
            return;
        }

        if (isAllImage) {
            drawImageShimmer(canvas);
        } else {
            drawShimmer(canvas);
        }

        localMaskBitmap = null;
    }

    private void drawShimmer(Canvas destinationCanvas) {
        createShimmerPaint(0);

        destinationCanvas.save();
        destinationCanvas.translate(maskOffsetX, 0);
        destinationCanvas.drawRect(maskRect.left, 0, maskRect.getWidth(), maskRect.getHeight(), gradientTexturePaint);
        destinationCanvas.restore();
    }

    private void drawImageShimmer(Canvas destinationCanvas) {
        createShimmerPaint(1);
        destinationCanvas.save();
        destinationCanvas.translate(maskOffsetX, 0);
        destinationCanvas.drawRect(maskRect.left, 0, maskRect.getWidth(), maskRect.getHeight(), gradientTexturePaint);
        destinationCanvas.restore();
    }

    private void resetShimmering() {
        if (maskAnimator != null) {
            maskAnimator.end();
            maskAnimator.release();
        }

        maskAnimator = null;
        gradientTexturePaint = null;
        isAnimationStarted = false;

        releaseBitMaps();
    }

    private void releaseBitMaps() {
        if (maskBitmap != null) {
            maskBitmap.release();
            maskBitmap = null;
        }
    }

    private PixelMap getMaskBitmap() {
        if (maskBitmap == null) {
            maskBitmap = createBitmap(maskRect.getWidth(), getHeight());
        }

        return maskBitmap;
    }

    private void createShimmerPaint(int type) {
        int edgeColor;
        if (type == 1) {
            edgeColor = Color.argb(0, Utils.redInt(shimmerColor),
                    Utils.greenInt(shimmerColor), Utils.blueInt(shimmerColor));
        } else {
            edgeColor = childBackgroundColor;
        }

        if (gradientTexturePaint != null) {
            return;
        }

        final float shimmerLineWidth = (float) getWidth() / 2 * maskWidth;
        final float positionY = (0 <= shimmerAngle) ? getHeight() : 0;

        Point[] points = new Point[POINT];
        points[0] = new Point(0, positionY);
        points[1] = new Point((float) Math.cos(Math.toRadians(shimmerAngle)) * shimmerLineWidth,
                positionY + (float) Math.sin(Math.toRadians(shimmerAngle)) * shimmerLineWidth);
        LinearShader gradient = new LinearShader(points,
                getGradientColorDistribution(),
                new Color[]{new Color(edgeColor),
                    new Color(shimmerColor),
                    new Color(shimmerColor),
                    new Color(edgeColor)},
                Shader.TileMode.CLAMP_TILEMODE);
        PixelMapShader maskBitmapShader = new PixelMapShader(new PixelMapHolder(localMaskBitmap),
                Shader.TileMode.CLAMP_TILEMODE, Shader.TileMode.CLAMP_TILEMODE);
        GroupShader groupShader = new GroupShader(gradient, maskBitmapShader, BlendMode.DST);

        gradientTexturePaint = new Paint();
        gradientTexturePaint.setShader(groupShader, Paint.ShaderType.GROUP_SHADER);
        gradientTexturePaint.setAntiAlias(true);
        gradientTexturePaint.setDither(true);
        gradientTexturePaint.setFilterBitmap(true);
        gradientTexturePaint.setBlendMode(BlendMode.SRC_IN);
    }

    private Animator getShimmerAnimation() {
        if (maskAnimator != null) {
            return maskAnimator;
        }

        if (maskRect == null) {
            maskRect = calculateBitmapMaskRect();
        }

        final int animationToX = getWidth();
        final int animationFromX;

        if (getWidth() > maskRect.getWidth()) {
            animationFromX = -animationToX;
        } else {
            animationFromX = -maskRect.getWidth();
        }

        final int shimmerBitmapWidth = maskRect.getWidth();
        final int shimmerAnimationFullLength = animationToX - animationFromX;
        maskAnimator = new AnimatorValue();
        maskAnimator.setDuration(shimmerAnimationDuration);
        maskAnimator.setLoopedCount(Animator.INFINITE);
        addDrawTask(this);
        maskAnimator.setValueUpdateListener((animatorValue, value) -> {
            int offsetx = isAnimationReversed ? (int) (shimmerAnimationFullLength * (1 - value))
                    : (int) (shimmerAnimationFullLength * value);
            HiLog.error(LABEL, "value" + value);
            maskOffsetX = animationFromX + offsetx;
            HiLog.error(LABEL, String.valueOf(maskOffsetX));
            HiLog.error(LABEL, "shimmerBitmapWidth" + shimmerBitmapWidth);
            if (maskOffsetX + shimmerBitmapWidth >= 0) {
                invalidate();
            }
        });
        return maskAnimator;
    }

    private PixelMap createBitmap(int width, int height) {
        try {
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(width, height);
            options.pixelFormat = PixelFormat.ARGB_8888;
            return PixelMap.create(options);
        } catch (OutOfMemoryError e) {
            return (PixelMap) Collections.emptyList();
        }
    }

    private Rect calculateBitmapMaskRect() {
        return new Rect(0, 0, calculateMaskWidth(), getHeight());
    }

    private int calculateMaskWidth() {
        final double shimmerLineBottomWidth = ((float)getWidth() / 2 * maskWidth)
                / Math.cos(Math.toRadians(Math.abs(shimmerAngle)));
        final double shimmerLineRemainingTopWidth = getHeight() * Math.tan(Math.toRadians(Math.abs(shimmerAngle)));

        return (int) (shimmerLineBottomWidth + shimmerLineRemainingTopWidth);
    }

    /**
     * getGradientColorDistribution
     *
     * @return float[]
     */
    private float[] getGradientColorDistribution() {
        final float[] colorDistribution = new float[DISTRIBUTION4];

        colorDistribution[0] = 0f;
        colorDistribution[DISTRIBUTION3] = 1.0f;

        colorDistribution[1] = DISTRIBUTION_FLOAT1 - gradientCenterColorWidth / DISTRIBUTION_FLOAT2;
        colorDistribution[DISTRIBUTION2] = DISTRIBUTION_FLOAT1 + gradientCenterColorWidth / DISTRIBUTION_FLOAT2;

        return colorDistribution;
    }

    public void setComponentContainer(ComponentContainer componentContainer) {
        mComponentContainer = componentContainer;
    }
}