package com.ethanhua.skeleton;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Created by ethanhua on 2017/7/29.
 *
 * @since 2021-04-16
 */
public class ViewReplacer {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ViewReplacer");
    private static final int INT = -1;
    private final Component mSourceView;
    private Component mTargetView;
    private int mTargetViewResId = INT;
    private Component mCurrentView;
    private ComponentContainer mSourceParentView;
    private final ComponentContainer.LayoutConfig mSourceViewLayoutParams;
    private int mSourceViewIndexInParent = 0;
    private final int mSourceViewId;

    /**
     * ViewReplacer
     *
     * @param sourceView
     */
    public ViewReplacer(Component sourceView) {
        mSourceView = sourceView;
        mSourceViewLayoutParams = mSourceView.getLayoutConfig();
        mCurrentView = mSourceView;
        mSourceViewId = mSourceView.getId();
    }

    /**
     * replace
     *
     * @param targetViewResId
     */
    public void replace(int targetViewResId) {
        if (mTargetViewResId == targetViewResId) {
            return;
        }
        if (init()) {
            mTargetViewResId = targetViewResId;
            replace(LayoutScatter.getInstance(
                    mSourceView.getContext()).parse(mTargetViewResId, mSourceParentView, false));
        }
    }

    /**
     * replace
     *
     * @param targetView
     */
    public void replace(Component targetView) {
        if (mCurrentView == targetView) {
            return;
        }
        if (targetView.getComponentParent() != null) {
            ((ComponentContainer) targetView.getComponentParent()).removeComponent(targetView);
        }
        if (init()) {
            mTargetView = targetView;
            mSourceParentView.removeComponent(mCurrentView);
            mTargetView.setId(mSourceViewId);
            mSourceParentView.addComponent(mTargetView, mSourceViewIndexInParent, mSourceViewLayoutParams);
            mCurrentView = mTargetView;
        }
    }

    /**
     * restore
     */
    public void restore() {
        if (mSourceParentView != null) {
            mSourceParentView.removeComponent(mCurrentView);
            mSourceParentView.addComponent(mSourceView, mSourceViewIndexInParent, mSourceViewLayoutParams);
            mCurrentView = mSourceView;
            mTargetView = null;
            mTargetViewResId = INT;
        }
    }

    public Component getSourceView() {
        return mSourceView;
    }

    public Component getTargetView() {
        return mTargetView;
    }

    public Component getCurrentView() {
        return mCurrentView;
    }

    private boolean init() {
        if (mSourceParentView == null) {
            mSourceParentView = (ComponentContainer) mSourceView.getComponentParent();
            if (mSourceParentView == null) {
                HiLog.info(LABEL, "the source view have not attach to any view");
                return false;
            }
            int count = mSourceParentView.getChildCount();
            for (int index = 0; index < count; index++) {
                if (mSourceView == mSourceParentView.getComponentAt(index)) {
                    mSourceViewIndexInParent = index;
                    break;
                }
            }
        }
        return true;
    }
}
