package com.ethanhua.skeleton;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.ListContainer;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

/**
 * RecyclerViewSkeletonScreen
 *
 * @since 2021-04-16
 */
public class RecyclerViewSkeletonScreen implements SkeletonScreen {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "RecyclerViewSkeletonScreen");
    private final ListContainer mRecyclerView;
    private final BaseItemProvider mActualAdapter;
    private final SkeletonAdapter mSkeletonAdapter;
    private final boolean isRecyclerViewFrozen;

    private RecyclerViewSkeletonScreen(Builder builder) {
        mRecyclerView = builder.mRecyclerView;
        mActualAdapter = builder.mActualAdapter;
        mSkeletonAdapter = new SkeletonAdapter();
        mSkeletonAdapter.setItemCount(builder.mItemCount);
        mSkeletonAdapter.setLayoutReference(builder.mItemResId);
        mSkeletonAdapter.setArrayOfLayoutReferences(builder.mItemsResIdArray);
        mSkeletonAdapter.shimmer(builder.isShimmer);
        mSkeletonAdapter.setShimmerColor(builder.mShimmerColor);
        mSkeletonAdapter.setShimmerAngle(builder.mShimmerAngle);
        mSkeletonAdapter.setShimmerDuration(builder.mShimmerDuration);
        mSkeletonAdapter.setChildBackgroundColor(builder.mChildBackgroundColor);
        isRecyclerViewFrozen = builder.isFrozen;
    }

    @Override
    public void show() {
        mRecyclerView.setItemProvider(mSkeletonAdapter);
        if (isRecyclerViewFrozen) {
            mRecyclerView.setEnabled(false);
        }
    }

    @Override
    public void hide() {
        mRecyclerView.setEnabled(true);
        mRecyclerView.setItemProvider(mActualAdapter);
    }

    /**
     * Builder
     *
     * @since 2021-04-16
     */
    public static class Builder {
        private static final int ITEMCOUNT = 10;
        private static final int SHIMMERDURATION = 1000;
        private static final int SHIMMERANGLE = 20;
        private BaseItemProvider mActualAdapter;
        private final ListContainer mRecyclerView;
        private boolean isShimmer = true;
        private int mItemCount = ITEMCOUNT;
        private int mItemResId = ResourceTable.Layout_list_default_item_skeleton;
        private int[] mItemsResIdArray;
        private int mShimmerColor;
        private int mShimmerDuration = SHIMMERDURATION;
        private int mShimmerAngle = SHIMMERANGLE;
        private boolean isFrozen = true;
        private int mChildBackgroundColor;

        /**
         * Builder
         *
         * @param recyclerView
         * @since 2021-04-16
         */
        public Builder(ListContainer recyclerView) {
            this.mRecyclerView = recyclerView;
            ohos.global.resource.ResourceManager resManager = recyclerView.getContext().getResourceManager();
            try {
                this.mShimmerColor = resManager.getElement(ResourceTable.Color_shimmer_color).getColor();
                this.mChildBackgroundColor = resManager.getElement(ResourceTable.Color_background_color).getColor();
            } catch (IOException | WrongTypeException | NotExistException e) {
                HiLog.info(LABEL,"");
            }
        }

        /**
         * whether show shimmer animation
         *
         * @param adapter the target recyclerView actual adapter
         * @return Builder
         */
        public Builder adapter(BaseItemProvider adapter) {
            this.mActualAdapter = adapter;
            return this;
        }

        /**
         * whether show shimmer animation
         *
         * @param itemCount the child item count in recyclerView
         * @return Builder
         */
        public Builder count(int itemCount) {
            this.mItemCount = itemCount;
            return this;
        }

        /**
         * whether show shimmer animation
         *
         * @param isShimmerShow whether show shimmer animation
         * @return Builder
         */
        public Builder shimmer(boolean isShimmerShow) {
            this.isShimmer = isShimmerShow;
            return this;
        }

        /**
         * the duration of the animation , the time it will take for the highlight to move from one end of the layout
         * to the other.
         *
         * @param shimmerDuration Duration of the shimmer animation, in milliseconds
         * @return Builder
         */
        public Builder duration(int shimmerDuration) {
            this.mShimmerDuration = shimmerDuration;
            return this;
        }

        /**
         * the angle of the shimmer effect in clockwise direction in degrees.
         *
         * @param shimmerColor the shimmer color
         * @return Builder
         */
        public Builder color(int shimmerColor) {
            this.mShimmerColor = shimmerColor;
            return this;
        }

        /**
         * the angle of the shimmer effect in clockwise direction in degrees.
         *
         * @param shimmerAngle the angle of the shimmer effect in clockwise direction in degrees.
         * @return Builder
         */
        public Builder angle(/*@IntRange(from = 0, to = 30)*/ int shimmerAngle) {
            this.mShimmerAngle = shimmerAngle;
            return this;
        }

        /**
         * the loading skeleton layoutResID
         *
         * @param skeletonLayoutResId the loading skeleton layoutResID\
         * @return Builder
         */
        public Builder load(int skeletonLayoutResId) {
            this.mItemResId = skeletonLayoutResId;
            return this;
        }

        /**
         * whether frozen recyclerView during skeleton showing
         *
         * @param isF whether frozen recyclerView during skeleton showing
         * @return Builder
         */
        public Builder frozen(boolean isF) {
            this.isFrozen = isF;
            return this;
        }

        /**
         * whether show background of childView
         *
         * @param backgroundColor whether show background of childView
         * @return Builder
         */
        public Builder childBackgroundColor(int backgroundColor) {
            this.mChildBackgroundColor = backgroundColor;
            return this;
        }

        /**
         * 显示列表
         *
         * @return RecyclerViewSkeletonScreen
         */
        public RecyclerViewSkeletonScreen show() {
            RecyclerViewSkeletonScreen recyclerViewSkeleton = new RecyclerViewSkeletonScreen(this);
            recyclerViewSkeleton.show();
            return recyclerViewSkeleton;
        }
    }
}
