package com.ethanhua.skeleton;

import io.supercharge.shimmerlayout.ShimmerLayout;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import java.util.Collections;

/**
 * Created by ethanhua on 2017/7/29.
 *
 * @since 2021-04-16
 */
public class SkeletonAdapter extends BaseItemProvider {
    private int mItemCount;
    private int mLayoutReference;
    private int[] mLayoutArrayReferences;
    private int mColor;
    private boolean isShimmer;
    private int mShimmerDuration;
    private int mShimmerAngle;
    private int mChildBackgroundColor;

    @Override
    public int getCount() {
        return mItemCount;
    }

    @Override
    public Object getItem(int i) {
        return Collections.emptyList();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        mLayoutReference = getCorrectLayoutItem(i);

        Component cpt = null;
        if (component == null) {
            LayoutScatter layoutScatter = LayoutScatter.getInstance(componentContainer.getContext());
            if (isShimmer) {
                cpt = ShimmerViewHolder.getComponent(layoutScatter, componentContainer, mLayoutReference);
            } else {
                cpt = LayoutScatter.getInstance(
                        componentContainer.getContext()).parse(mLayoutReference, componentContainer, false);
            }
        } else {
            cpt = component;
        }
        if (isShimmer) {
            ShimmerLayout layout = (ShimmerLayout) cpt;
            layout.setShimmerAnimationDuration(mShimmerDuration);
            layout.setShimmerAngle(mShimmerAngle);
            layout.setShimmerColor(mColor);
            layout.setChildBackgroundColor(mChildBackgroundColor);
            layout.startShimmerAnimation();
        }

        return cpt;
    }

    public void setLayoutReference(int layoutReference) {
        this.mLayoutReference = layoutReference;
    }

    public void setArrayOfLayoutReferences(int[] layoutReferences) {
        if (layoutReferences != null) {
            this.mLayoutArrayReferences = layoutReferences.clone();
        }
    }

    public void setItemCount(int itemCount) {
        this.mItemCount = itemCount;
    }

    public void setShimmerColor(int color) {
        this.mColor = color;
    }

    /**
     * shimmer
     *
     * @param isShimmerShow
     */
    public void shimmer(boolean isShimmerShow) {
        this.isShimmer = isShimmerShow;
    }

    public void setShimmerDuration(int shimmerDuration) {
        this.mShimmerDuration = shimmerDuration;
    }

    public void setChildBackgroundColor(int color) {
        this.mChildBackgroundColor = color;
    }

    public void setShimmerAngle(/*@IntRange(from = 0, to = 30)*/ int shimmerAngle) {
        this.mShimmerAngle = shimmerAngle;
    }

    /**
     * getCorrectLayoutItem
     *
     * @param position
     * @return int
     */
    public int getCorrectLayoutItem(int position) {
        if (doesArrayOfLayoutsExist()) {
            return mLayoutArrayReferences[position % mLayoutArrayReferences.length];
        }
        return mLayoutReference;
    }

    private boolean doesArrayOfLayoutsExist() {
        return mLayoutArrayReferences != null && mLayoutArrayReferences.length != 0;
    }
}
