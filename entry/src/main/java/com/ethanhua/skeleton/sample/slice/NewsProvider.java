package com.ethanhua.skeleton.sample.slice;

import com.ethanhua.skeleton.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import java.util.Collections;

/**
 * NewsProvider
 *
 * @since 2021-04-16
 */
public class NewsProvider extends BaseItemProvider {
    private static final int COUNT = 10;
    private AbilitySlice slice;

    /**
     * NewsProvider
     *
     * @param slice
     */
    public NewsProvider(AbilitySlice slice) {
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public Object getItem(int i) {
        return Collections.emptyList();
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        return LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_news, null, false);
    }
}
