package com.ethanhua.skeleton.sample.slice;

import com.ethanhua.skeleton.sample.ResourceTable;
import com.ethanhua.skeleton.utils.DensityUtils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

import java.util.Collections;

/**
 * PersonProvider
 *
 * @since 2021-04-16
 */
public class PersonProvider extends BaseItemProvider {
    private static final int COUNT = 10;
    private static final int INTEGER = 2;
    private AbilitySlice slice;

    /**
     * PersonProvider
     *
     * @param slice
     */
    public PersonProvider(AbilitySlice slice) {
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public Object getItem(int i) {
        return Collections.emptyList();
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component com = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_person, null, false);

        int width = DensityUtils.getDisplayWidth(slice);

        int cloum = width / INTEGER;
        Text tvTime = (Text) com.findComponentById(ResourceTable.Id_tv_time);
        tvTime.setText("Fast & Furious");
        com.setWidth(cloum);

        return com;
    }
}
