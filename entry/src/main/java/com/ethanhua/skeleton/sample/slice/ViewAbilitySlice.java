package com.ethanhua.skeleton.sample.slice;

import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;
import com.ethanhua.skeleton.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.components.ScrollView;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * ViewAbilitySlice
 *
 * @since 2021-04-16
 */
public class ViewAbilitySlice extends AbilitySlice {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ViewAbilitySlice");

    private static final String PARAMS_TYPE = "params_type";
    private static final String TYPE_IMG_LOADING = "type_img";
    private static final String TYPE_VIEW = "type_view";
    private static final int TITLE_COLOR = -13615201;
    private static final int DURATION = 1200;
    private static final int TIME = 3000;

    private ScrollView mScrollView;

    private String mType = "";
    private SkeletonScreen skeletonScreen;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_view);

        mScrollView = (ScrollView) findComponentById(ResourceTable.Id_sv);
        mType = intent.getStringParam(PARAMS_TYPE);

        initView();
    }

    private void initView() {
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        TopicProvider topicProvider = new TopicProvider(this);
        listContainer.setItemProvider(topicProvider);
        if (TYPE_VIEW.equals(mType)) {
            skeletonScreen = Skeleton.bind(mScrollView)
                    .load(ResourceTable.Layout_activity_view_skeleton)
                    .duration(DURATION)
                    .color(Color.getIntColor("#EAEAEA"))
                    .shimmer(true)
                    .angle(0)
                    .show();
        } else {
            skeletonScreen = Skeleton.bind(mScrollView)
                    .load(ResourceTable.Layout_dialog)
                    .duration(DURATION)
                    .color(Color.getIntColor("#55efefef"))
                    .shimmer(true)
                    .show();
        }

        HiLog.info(LABEL, "getMainTaskDispatcher() start ");
        ViewAbilitySlice.this.getMainTaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                skeletonScreen.hide();
                HiLog.info(LABEL, "getMainTaskDispatcher()");
            }
        }, TIME);
        HiLog.info(LABEL, "getMainTaskDispatcher() end");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
