package com.ethanhua.skeleton.sample;

import com.ethanhua.skeleton.sample.slice.StatusViewSlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;


/**
 * StatusViewAbility
 *
 * @since 2021-04-16
 */
public class StatusViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(StatusViewSlice.class.getName());
        onLeaveForeground();
    }
}
